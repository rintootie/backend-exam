
from http.server import BaseHTTPRequestHandler,HTTPServer, SimpleHTTPRequestHandler
import socketserver, os
import services, url_routes

def get_function(path):
    """
        Gets the corresponding function to be executed given a url
        Args:
            path: url string 
        Returns:
            dictionary with keys "name" and "og_path"
            name contains the function name to be executed
            og_path contains the path with the '/api' stripped
    """
    raw_url = os.path.normpath(path)
    get_path = raw_url.split(os.sep)[2:]
    return {'name': url_routes.routes_list.get(get_path[0], None), 'og_path': get_path}

class GetHandler(SimpleHTTPRequestHandler):
        
        def do_GET(self):
            # search if the url entered has a corresponding function, otherwise respond with 404 
            function = get_function(self.path)
            if function.get('name') is not None:
                data = (eval(function.get('name')))("GET", function.get('og_path'))
                self.send_response(200)
                self.send_header('Content-Type', 'application/json')
                self.end_headers()
                self.wfile.write(data.encode(encoding='utf_8'))
            else:
                self.send_response(404, 'Not found')
            self.end_headers()


        def do_POST(self):
            # get posted data
            length = int(self.headers.get('content-length'))
            rfile_str = self.rfile.read(length).decode('utf8')
            # search if the url entered has a corresponding function, otherwise respond with 404 
            function = get_function(self.path)
            if function.get('name') is not None:
                data = (eval(function.get('name')))("POST", function.get('og_path'), data = rfile_str)
                if data.get("success") == True:
                    self.send_response(data.get("response_code"))
                else:
                    self.send_response(422)
                self.send_header('Content-Type', 'application/json')
                self.end_headers()
                self.wfile.write(data.get("data").encode(encoding='utf_8'))
            else:
                self.send_response(404, 'Not found')
            self.end_headers()
            

"""
==============
RUNNING SERVER
==============
"""

Handler=GetHandler
if __name__ == '__main__':
    try:
        httpd=socketserver.TCPServer(('', 8000), Handler)
        httpd.serve_forever()   
    except KeyboardInterrupt:
        print(f'\nShutting down server.')
        httpd.socket.close()