import sqlite3, os
from sqlite3 import Error

basepath = os.path.dirname(__file__)
DATABASE_NAME = "sampledb.db"

def create_con(db = None):
    """
        Creates a database connection to the SQLite database

        Args:
            db: SQLite database file
        
        Returns:
            Connection object if exists
            otherwise None
    """
    conn = None
    try:
        if db is None:
            db = DATABASE_NAME
        conn = sqlite3.connect(db)
        return conn
    except Error as e:
        print(e)
    return conn


def create_table(conn, sql):
    """
        Creates a table given an SQL statement

        Args:
            conn: Connection object
            sql : SQL statement
    """
    try:
        c = conn.cursor()
        c.execute(sql)
    except Error as e:
        print(e)


def main():    
    create_users_table = """ CREATE TABLE IF NOT EXISTS users (
                                        id integer PRIMARY KEY AUTOINCREMENT,
                                        email text UNIQUE,
                                        name text NOT NULL,
                                        password text NOT NULL,
                                        created_at datetime DEFAULT (datetime('now','localtime')),
                                        updated_at datetime DEFAULT (datetime('now','localtime'))
                                    ); """
    
    create_articles_table = """ CREATE TABLE IF NOT EXISTS articles (
                                        id integer PRIMARY KEY AUTOINCREMENT,
                                        title text,
                                        slug text UNIQUE NOT NULL,
                                        content text,
                                        updated_at datetime DEFAULT (datetime('now','localtime')),
                                        created_at datetime DEFAULT (datetime('now','localtime')),
                                        user_id integer NOT NULL,
                                        FOREIGN KEY(user_id) REFERENCES users(id)
                                    );
                            """
    # create database connection
    conn = create_con(DATABASE_NAME)
    if conn is not None:
        create_table(conn, create_users_table)
        create_table(conn, create_articles_table)
    else:
        print("Cannot create the database connection.")


if __name__ == '__main__':
    main()