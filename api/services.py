import json, os.path, re, datetime, base64
import dbcon, dbqueries

"""
    ===================================
    FUNCTIONS CALLED FROM url_routes.py
    ===================================
"""
def posts(type, path, data = None):
    """
        Args:
            type: post type
            path: list (derived from the url but is divided into elements)
            data: submitted data (from POST requests)
        Returns:
            if GET, json of result
            if POST, dictionary with keys "success", "response_code", and "data"
                success: boolean
                response code: int
                data: json
    """
    connection = dbcon.create_con()
    if len(path) > 1:
        fields = ['slug']
        conditions = [path[1]]
    # REQUEST TYPE IS POST
    if type == "GET":
        if len(path) > 1:
            results = dbqueries.get(connection, "articles", where_field = fields, where_value = conditions)
        else:
            results = dbqueries.get(connection, "articles")
    # REQUEST TYPE IS POST
    elif type == "POST" and data is not None:
        results = {}
        results["success"] = False
        data = json.loads(data)
        data["updated_at"] = datetime.datetime.now()
        if len(path) > 1:
            # for updating post
            results = dbqueries.update(connection, "articles", set_field = list(data.keys()), set_value = list(data.values()), where_field = fields, where_value = conditions)
            if results.get("success")== True:
                results["success"] = True
                results["response_code"] = 200
            results["data"] = results.get("data")
        else:
            # create article not included
           results["data"] = dict_to_json({"message":"The given data was invalid", "errors": {"email": "Create article not included"}})
    return results

def register(type, path, data):
    """
        Args:
            type: post type
            path: list (derived from the url but is divided into elements)
            data: submitted data (from POST requests)
        Returns:
            dictionary with keys "success", "response_code", and "data"
                success: boolean
                response code: int
                data: json
    """
    details = json.loads(data)
    user_fields = ["email", "name", "password","password_confirmation"]
    valid = validate_user_details(details, user_fields)
    results = {} 
    results["success"] = False
    if valid.get("is_valid"):
        details.pop("password_confirmation")
        details["password"] = encode(details.get("password"))
        connection = dbcon.create_con()
        query = dbqueries.insert(connection, "users", values = list(details.values()), columns = list(details.keys()))
        if query.get("success")== True:
            results["success"] = True
            results["response_code"] = 201
        results["data"] = query.get("data")
    else:
        results["data"] = dict_to_json({"message":"The given data was invalid", "errors": valid.get("errors")})
    return results

def login(type, path, data):
    """
        Args:
            type: post type
            path: list (derived from the url but is divided into elements)
            data: submitted data (from POST requests)
        Returns:
            dictionary with keys "success", "response_code", and "data"
                success: boolean
                response code: int
                data: json
    """
    results = {}
    results["success"] = False
    details = json.loads(data)
    user_fields = ["email", "password"]
    initial = validate_user_details(details, user_fields)
    connection = dbcon.create_con()
    if initial.get("is_valid"):
        email_check = check_if_exists("users", ["email"], [details.get("email")])
        if not email_check:
            results["data"] = dict_to_json({"message":"The given data was invalid", "errors": {"email": "These credentials do not match our records."}})
        else:
            fields = ['email', 'password']
            conditions = [ details.get("email"), encode(details.get("password"))]
            query = check_if_exists("users", where_value= conditions, where_field = fields)
            if query:
                results["success"] = True
                results["response_code"] = 200
                results["data"] = dict_to_json({"token": "To add token here"})
            else:
                results["data"] = dict_to_json({"message":"The given data was invalid", "errors": {"email": "Email does not match the password."}})
            pass
    return results

"""
    =================
    UTILITY FUNCTIONS
    =================
"""

def dict_to_json(dict):
    """
        Converts a dictionary to json
        Args:
            dict: dictionary object
        Returns:
            json object
    """
    return json.dumps(dict)

def validate_user_details(data, user_fields):
    """
        Validates user fields given data
        Args:
            data: dictionary object
        Returns:
            dictionary containing two keys: "is_valid" and "errors"
    """

    flag = True
    errors = []

    for field in user_fields:
        if data.get(field) is None or data.get(field) == '':
            flag = False
            errors.append({f"{field}": f"The {field} is required"})
    
    if data.get("password_confirmation", None) is not None:
        if  data.get("password") != data.get("password_confirmation"):
            flag = False
            errors.append({"password": "The password confirmation does not match."})

    if not is_email(data.get("email")):
        flag = False
        errors.append({"email": "The email is invalid."})

    data = {"is_valid": flag, "errors": errors}
    return data

def encode(string):
    """
        Encodes string to base64
        Args:
            data: dictionary object
        Returns:
            dictionary containing two keys: "is_valid" and "errors"
    """
    # To change to a better algo later on
    # Used for the sake of not saving the plain text pw in the db
    return str(base64.urlsafe_b64encode(string.encode("utf-8")), "utf-8")


def check_if_exists(table, where_field, where_value):
    """
        Checks if value exists in a given table
        Args:
            table: table name
            where_field: list object that contains the fields to be placed in the where condition
            where_value: list object that contains the counterpart values for the where fields
        Returns:
            dictionary containing two keys: "is_valid" and "errors"
    """
    connection = dbcon.create_con()
    result = dbqueries.get(connection, table, where_field = where_field, where_value = where_value) 
    return len(json.loads(result)) > 0

def is_email(string):
    """
        Checks if given string is a valid email
        Args:
            string: email string to check
        Returns:
            boolean
    """
    regex = '^[a-z0-9]+[\._]?[a-z0-9]+[@]\w+[.]\w{2,3}$'
    return re.search(regex,string)

