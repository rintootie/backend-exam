#### Steps:

- run server with this command: python server.py
- create database with this command: python dbcon.py

##### Implemented Endpoints:
*body data format followed from: https://documenter.getpostman.com/view/78990/RznLHGgv*
- api/posts/
-- retrieves all posts
- api/posts/<slug-name>
-- GET: retrieves specific record given slug name
-- POST: updates specific record given slug name
- api/register
-- registers users
--* no encryption on password
- api/login
-- no token returned after authentication