import sqlite3
import services

def dict_factory(cursor, row):
    """
        Code from python documentation: https://docs.python.org/3/library/sqlite3.html#sqlite3.Connection.row_factory
        Adds the column name to the results
    """
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d

def get(dbcon, table, where_field = None, where_value = None):
    """
        Retrieves record/s from the database
        Args:
            dbcon: Connection object
            where_field: list object that contains the fields to be placed in the where condition
            where_value: list object that contains the counterpart values for the where fields
        Returns:
            json object of the result with column names
    """

    dbcon.row_factory = dict_factory

    sql = f"SELECT * FROM {table}"
   
    if where_field is not None:
        length = len(where_field)
        sql += " WHERE "
        for x in range(length):
            sql += f"{ where_field[x] } = '{ where_value[x] }'"
            if x != length - 1:
                sql += " AND "

    print(sql)
    cursor = dbcon.execute(sql)
    res = cursor.fetchall()
    json = services.dict_to_json(res)
    dbcon.close()
    return json

def update(dbcon, table, set_field, set_value, where_field = None, where_value = None):
    """
        Retrieves record/s from the database
        Args:
            dbcon: Connection object
            set_field  : list object that contains the fields to be updated
            set_value  : list object that contains the counterpart values for the set fields
            where_field: list object that contains the fields to be placed in the where condition
            where_value: list object that contains the counterpart values for the where fields
        Returns:
            json object of the updated record with column names
    """
    sql = f"UPDATE {table} SET "

    length = len(set_field)
    for y in range(length):
        sql += f"{set_field[y]} = '{set_value[y]}'"
        if y != length -1:
            sql += ","

    if where_field is not None:
        length = len(where_field)
        sql += " WHERE "
        for x in range(length):
            sql += f"{ where_field[x] } = '{ where_value[x] }'"
            if x != length - 1:
                sql += " AND "
    
    print(sql)
    

    try:
        cursor = dbcon.cursor()
        cursor.execute(sql)
        dbcon.commit()
        return {"success": True, "data": get(dbcon, table, where_field=where_field, where_value=where_value)}
    except Exception as error:
        dicti = {"errors": str(error)}
        return {"success": False, "data": services.dict_to_json(dicti)}

def insert(dbcon, table, values, columns = None):
    """
        Inserts record to the database
        Args:
            dbcon: Connection object
            values   : list object that contains the values to be inserted
            columns  : list object that contains the chosen columns when inserting
        Returns:
            json object of the inserted record with column names
    """
    if columns == None:
        sql = f"INSERT INTO {table} values {values}"
    else:
        sql = f"INSERT INTO {table}(" 
        length = len(columns)
        for x in range(length):
            sql += f"{columns[x]}"
            if x != length - 1:
                sql += " , "
            else:
                sql += ") "
        sql += " VALUES ("
        for x in range(length):
            sql += f"'{values[x]}'"
            if x != length - 1:
                sql += " , "
            else:
                sql += ") "
    print(sql)
    results = {}
    try:
        cursor = dbcon.cursor()
        cursor.execute(sql)
        dbcon.commit()
        last_id = cursor.lastrowid
        data = get(dbcon, table, where_field=["id"], where_value=[last_id])
        return {"success": True, "data": data}
    except Exception as error:
        dicti = {"errors": str(error)}
        return {"success": False, "data": services.dict_to_json(dicti)}